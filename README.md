# R2D2 - Automatische Generierung von Schul-Mappen

Durch die Digitalisierung haben wir immer mehr PDFs und einzelne digitale Blätter auf unseren Rechnern liegen und sollen dann wieder die _klassischen_ Mappen erstellen.

Hierzu müssten wir die Blätter alle ausdrucken, um sie anschließend abzuheften und nach der Bewertung durch die Lehrer wegzuwerfen.

Das ist nicht im Sinne einer umweltfreundlichen Bearbeitung und hat auch nichts mit Digitalisierung zu tun.

Ich habe hier ein kleines Programm geschrieben, welches dazu gedacht ist aus den einzelnen Blättern in einem Verzeichnis eine _digitale_ Mappe, als PDF zu generieren.

# Installation

Das Programm wurde so entwickelt, dass es auf möglichst vielen unterschiedlichen Systemen lauffähig ist. Hierzu wurde als Programmiersprache JAVA verwendet. Um das Programm nutzen zu können müsst ihr eine lauffähige JAVA Umgebung (JRE/JDK) auf eurem Rechner installiert haben.

Ob JAVA bereits installiert ist könnt ihr prüfen, indem ihr in der Kommandozeile (in Windows cmd.exe) folgendes eingebt:

    java -version

Als Ausgabe erhaltet ihr dann entweder etwas wie dieses hier:

    openjdk version "15.0.3" 2021-04-20
    OpenJDK Runtime Environment (build 15.0.3+3-suse-1.1-x8664)
    OpenJDK 64-Bit Server VM (build 15.0.3+3-suse-1.1-x8664, mixed mode)

Das bedeutet, dass ihr JAVA installiert habt. Falls dies nicht der Fall ist, empfehlen wir die Installation einer auf openjdk basierenden, aktuellen Version von JAVA. Unter Linux findet ihr sicher eine in den entsprechenden Installationsquellen. Unter Windows 10 haben wir unsere Software mit Amazon Corretto getestet. Diesen JDK bekommt ihr sowohl für Windows als auch Macintosh. Details und Links dazu im folgenden Kapitel.

## Amazon Corretto 16

Amazon Corretto ist eine kostenlose, plattformübergreifende Distribution des Open Java Development Kit (OpenJDK). Corretto wird intern bei Amazon produktiv verwendet. Mit Corretto lassen sich Java-Anwendungen auf Betriebssystemen wie Amazon Linux 2, Windows und macOS entwickeln und ausführen.

Die neuesten Amazon Corretto 16 Release-Builds können hier heruntergeladen werden: [https://github.com/corretto/corretto-16/releases](https://github.com/corretto/corretto-16/releases)

Eine Dokumentation des JDK ist hier zu finden: [https://docs.aws.amazon.com/corretto](https://docs.aws.amazon.com/corretto).

## Der Installer

Damit ihr euch nach der Installation von JAVA nicht noch um die Dateiabhängigkeiten kümmern müsst, haben wir die Software mit allem, was erforderlich ist in einem Installer zusammengefasst. Diesen Installer könnt ihr herunterladen direkt ausführen. Er wird alles Erforderliche dann schrittweise mit euch installieren.

> **_HINWEIS:_**  Zur Installation der Software sind __keine__ Administrator-Rechte erforderlich.

---