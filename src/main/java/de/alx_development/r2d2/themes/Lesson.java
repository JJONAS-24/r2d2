package de.alx_development.r2d2.themes;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import java.io.File;
import java.nio.file.Paths;

public abstract class Lesson {

    private final String key;
    private final String localizedName;
    private Theme theme;

    public Lesson(String key, String localizedName) {
        this.key = key;
        this.localizedName = localizedName;
    }

    /**
     * This function returns the key identifier for the lesson. The
     * key identifier is language independent and used to identify
     * a lesson.
     *
     * @return The lesson key
     */
    public String getKey() {
        return key;
    }

    /**
     * To get a language dependent string for the lesson, use this
     * function. If defined in the resources a string name according the
     * systems locale is returned.
     *
     * @return A localized name as string
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     * This method should be called by the subclasses to set the internal
     * reference to the theme which owns this lesson.
     *
     * @param theme The theme this lesson belongs to
     */
    protected void setTheme(Theme theme) {
        this.theme = theme;
    }

    /**
     * This function returns the reference to the theme where this lesson
     * is controlled.
     *
     * @return Reference to the lessons theme
     */
    public Theme getTheme() {
        return theme;
    }

    /**
     * This function returns a file reference to the cover page for this lesson
     *
     * @return <code>File</code> reference to the cover page
     */
    public File getCoverFile() {
        return Paths.get(theme.getManager().getThemeDirectory().getAbsolutePath(),
                getTheme().getId(),
                "cover",
                String.format("%s.pdf", getKey())
        ).toFile();
    }

    /**
     * This function has been overridden to just return the localized
     * name for use in <code>JComboBox</code> components or other standard
     * elements which use the <code>toString()</code> function to get an
     * object name.
     *
     * @return The localized name
     * @see #getLocalizedName()
     */
    @Override
    public String toString() {
        return getLocalizedName();
    }
}
