package de.alx_development.r2d2.themes;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.util.Enumeration;
import java.util.logging.Logger;

public class ThemeSelector extends JPanel {

    protected Logger logger = Logger.getLogger(getClass().getName());

    // The JLabel which displays the themes information
    private final JLabel themesInfo = new JLabel();
    private final JComboBox<Theme> themesList = new JComboBox<>();
    private final JComboBox<Lesson> lessonsList = new JComboBox<>();

    /**
     * Default constructor which expects the <code>ThemeManager</code> which
     * is handled by the selector gui.
     *
     * @param manager The manager which is controlled by this gui component
     * @see ThemeManager
     */
    public ThemeSelector(ThemeManager manager) {
        super();
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        // The JComboBox is used to select the theme and switch between the
        // available themes
        themesList.setModel(manager);
        themesList.addActionListener(e -> {
            Theme theme = (Theme) themesList.getSelectedItem();
            assert theme != null;
            setTheme(theme);
        });
        setTheme(manager.getSelectedItem());

        lessonsList.addActionListener(e -> {
            Lesson lesson = (Lesson) lessonsList.getSelectedItem();
            logger.config(String.format("Lesson [%s] activated", lesson));
        });

        JPanel lessonsSelector = new JPanel();
        GridLayout experimentLayout = new GridLayout(0,2);
        lessonsSelector.setLayout(experimentLayout);
        lessonsSelector.add(themesList);
        lessonsSelector.add(lessonsList);

        add(lessonsSelector, BorderLayout.NORTH);
        add(themesInfo, BorderLayout.CENTER);
    }

    /**
     * Internal used function to format the themes information label.
     *
     * @param theme The theme which should be displayed
     */
    private void setTheme(Theme theme) {

        // Setting up the Information label
        themesInfo.setText(
                String.format("<html>" +
                                "<table><tr>" +
                                "<td><h1>%s</h1></td>" +
                                "<td><p>%s<br/><i>Autor: %s</i></p></td><" +
                                "/tr></table>" +
                                "</html>",
                theme.getProperty("name"),
                theme.getProperty("description"),
                theme.getProperty("author"))
        );

        // Setting up the lesson selector
        lessonsList.removeAllItems();
        Enumeration<String> lessons = theme.getLessons();
        while(lessons.hasMoreElements()) {
            String key = lessons.nextElement();
            lessonsList.addItem(theme.getLesson(key));
        }
    }

    /**
     * This function returns the selected <code>Theme</code> object.
     *
     * @return selected theme
     */
    public Theme getSelectedTheme() {
        return (Theme) themesList.getSelectedItem();
    }

    /**
     * This function returns the selected <code>Lesson</code> object
     *
     * @return selected lesson
     */
    public Lesson getSelectedLesson() {
        return (Lesson) lessonsList.getSelectedItem();
    }
}
