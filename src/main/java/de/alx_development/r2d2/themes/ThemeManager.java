package de.alx_development.r2d2.themes;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import javax.swing.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ThemeManager extends AbstractListModel<Theme> implements ComboBoxModel<Theme> {

    protected Logger logger = Logger.getLogger(getClass().getName());
    private final LinkedHashMap<String, Theme> themes = new LinkedHashMap<>();
    private Theme activeTheme = null;
    private File themeDirectory;

    public ThemeManager() {
        this(Paths.get(".", "themes").toFile());
    }

    public ThemeManager(File dir) {
        setThemeDirectory(dir);

        logger.info("Loading themes from [" + getThemeDirectory().getAbsolutePath() + "]");
        Set<String> directoryListing = Stream.of(Objects.requireNonNull(getThemeDirectory().listFiles()))
                .filter(File::isDirectory)
                .map(File::getName)
                .collect(Collectors.toSet());

        // Iterating over the themes directory content
        for (String s : directoryListing) {
            addTheme(new ManagedTheme(s, this));
        }

        // Setting the first theme as active if themes have been registered
        if(getSize() > 0)
            setSelectedItem(getElementAt(0));
    }

    /**
     * Internally used function to register a new theme at the <code>ThemeManager</code>
     *
     * @param theme The theme which should be added.
     */
    private void addTheme(Theme theme) {
        if(theme != null) {
            logger.info("Registering new theme [" + theme.getId() + "]");
            themes.put(theme.getId(), theme);
        }
    }

    /**
     * This function returns the theme directory which is used as basis
     * for the managed themes by this <code>ThemeManager</code>.
     *
     * @return File reference to the themes directory
     */
    public File getThemeDirectory() {
        return themeDirectory;
    }

    /**
     * Internally used function to set teh themes directory. If necessary, you
     * may override this function if you like to change the directory in derived
     * classes.
     * If the passed <code>File</code> object is not a directory the call is ignored
     * and the theme directory is not changed.
     *
     * @param dir The new themes directory to use
     */
    private void setThemeDirectory(File dir)  {
        if(dir.isDirectory()){
            this.themeDirectory = dir;
        }
    }

    /**
     * Returns the number of themes actually controlled by this
     * <code>ThemeManager</code> instance.
     *
     * @return  the number of themes in this list
     */
    @Override
    public int getSize() {
        return themes.size();
    }

    /**
     * Returns the <code>Theme</code> object stored at the specified index.
     *
     * @param      index   an index into this list
     * @return     the theme at the specified index
     * @throws     ArrayIndexOutOfBoundsException  if the {@code index} is negative or greater than the current size of this list
     */
    @Override
    public Theme getElementAt(int index) {
        String[] keys = themes.keySet().toArray(new String[0]);
        return themes.get(keys[index]);
    }

    /**
     * This function is setting a <code>Theme</code> as active, if the given theme
     * is controlled by this <code>ThemeManager</code>.
     *
     * @param activeTheme The theme which should be activated
     */
    @Override
    public void setSelectedItem(Object activeTheme) {
        if(themes.containsValue(activeTheme)) {
            this.activeTheme = (Theme) activeTheme;
            logger.info("Theme [" + ((Theme) activeTheme).getId() + "] activated");
        }
    }

    /**
     * This function returns the actual activated <code>Theme</code>.
     *
     * @return The activated theme
     */
    @Override
    public Theme getSelectedItem() {
        return activeTheme;
    }
}

class ManagedTheme extends Theme {

    /**
     * Default constructor expecting the themes id to initialize and
     * load the theme. If no theme can be found with the id a <code>null</code>
     * reference will be created.
     *
     * @param id The theme id which should be initialized
     * @param manager Reference to the controlling <code>ThemeManager</code>
     */
    public ManagedTheme(String id, ThemeManager manager) {
        super(id);
        setThemeManager(manager);
    }

}
