package de.alx_development.r2d2.themes;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public abstract class Theme {

    private String id;

    private final Path baseDirectory;
    private ThemeManager manager;
    private ResourceBundle properties;
    private ResourceBundle lessons;

    protected Logger logger = Logger.getLogger(getClass().getName());

    /**
     * A constructor which should be extended by a concrete implementation
     *
     * @param id The id of the theme
     */
    public Theme(String id) {
        super();
        setId(id);

        logger.config(String.format("Initializing theme [%s]", id));
        baseDirectory = Paths.get("themes", getId());

        try {
            URL[] urls = new URL[]{baseDirectory.toUri().toURL()};
            ClassLoader loader = new URLClassLoader(urls);
            properties = ResourceBundle.getBundle("theme", Locale.getDefault(), loader);
            lessons = ResourceBundle.getBundle("lessons", Locale.getDefault(), loader);
            logger.config(String.format("Resource information for theme [%s] loaded from [%s]",
                    getProperty("name"),
                    baseDirectory.toAbsolutePath()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function returns a string enumeration of the design covers (lessons).
     *
     * @return String enumeration of lessons keys
     */
    public Enumeration<String> getLessons() {
        return lessons.getKeys();
    }

    /**
     * This function returns the lessons name as defined in the localized resource
     *
     * @param key The lessons key
     * @return The language dependent name of the lesson
     */
    public Lesson getLesson(String key) {
        Lesson lesson = new ManagedLesson(this, key, lessons.getString(key));
        return lesson;
    }

    /**
     * This function may be internally used to change the id if necessary.
     * If the passed argument is null it will be ignored and the id is not
     * changed.
     *
     * @param id The <code>String</code> which should be used as new id
     */
    protected void setId(String id) {
        if(id != null) {
            this.id = id;
        }
    }
    /**
     * This function returns the key which identifies the theme inside
     * the <code>ThemeManager</code>.
     *
     * @return The themes id
     */
    public String getId() {
        return id;
    }

    /**
     * This function can be used to set the reference to a <code>ThemeManager</code> object.
     *
     * @param manager The <code>ThemeManager</code> which handles this <code>Theme</code>
     * @see ThemeManager
     */
    protected void setThemeManager(ThemeManager manager) {
        this.manager = manager;
    }

    /**
     * This function returns a reference to the <code>ThemeManager</code> object which
     * handles this <code>Theme</code>
     *
     * @return Reference to the <code>ThemeManager</code>
     * @see ThemeManager
     */
    public ThemeManager getManager() {
        return manager;
    }

    /**
     * Use this function to get the localized property information for this theme. If
     * there is no property available with the given key, the key is returned
     *
     * @return Reference to properties list
     */
    public String getProperty(String key) {
        return properties.getString(key);
    }

    /**
     * Returns a string representation of the object. In general, the toString method returns a string that
     * "textually represents" this object. The result should be a concise but informative representation that
     * is easy for a person to read. It is recommended that all subclasses override this method.
     *
     * @return Returns a string representation of the theme object.
     */
    @Override
    public String toString() {
        return getProperty("name");
    }
}

class ManagedLesson extends Lesson {

    public ManagedLesson(Theme theme, String key, String localizedName) {
        super(key, localizedName);
        setTheme(theme);
    }
}
