package de.alx_development.r2d2;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import de.alx_development.application.Bootstrapper;
import de.alx_development.application.JApplicationFrame;
import de.alx_development.r2d2.documents.DocumentSelector;
import de.alx_development.r2d2.documents.OutputSelector;
import de.alx_development.r2d2.themes.ThemeManager;
import de.alx_development.r2d2.themes.ThemeSelector;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Objects;

public class R2D2 extends JApplicationFrame {

    public static void main(String[] args)
    {
        Bootstrapper.load(R2D2.class);
    }

    public R2D2() {
        super();
        setTitle("R2D2");
        setApplicationIcon(new ImageIcon(Objects.requireNonNull(R2D2.class.getResource("icon.png"))));

        ThemeSelector themeSelector = new ThemeSelector(new ThemeManager());
        DocumentSelector documentSelector = new DocumentSelector();
        OutputSelector outputSelector = new OutputSelector();

        JButton generateOutput = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("system-run.png"))));
        generateOutput.setToolTipText("Ausgabe erzeugen");
        generateOutput.addActionListener(e -> {
            try {
                documentSelector.getDocumentListModel().merge(outputSelector.getFile(), themeSelector.getSelectedLesson());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        JPanel themesPanel = new JPanel();
        themesPanel.setLayout(new BorderLayout());
        themesPanel.add(generateOutput, BorderLayout.EAST);
        themesPanel.add(themeSelector, BorderLayout.CENTER);

        add(themesPanel, BorderLayout.NORTH);
        add(documentSelector, BorderLayout.CENTER);
        add(outputSelector, BorderLayout.SOUTH);
    }

    /**
     * Terminates the currently running Application and is closing any external
     * connections.
     *
     * @param status exit status.
     */
    @Override
    public void exit(int status) {
        // Doing everything the application itself needs to do
        super.exit(status);
    }
}
