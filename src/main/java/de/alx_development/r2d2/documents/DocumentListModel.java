package de.alx_development.r2d2.documents;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import de.alx_development.r2d2.themes.Lesson;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Objects;
import java.util.logging.Logger;

public class DocumentListModel extends AbstractListModel<Document> {

    protected Logger logger = Logger.getLogger(getClass().getName());
    private final LinkedList<Document> documentList = new LinkedList<>();

    /**
     * Default constructor which doesn't accept any arguments,
     * but is initializing the list.
     */
    public DocumentListModel() {
        super();
    }

    /**
     * Returns the length of the list.
     *
     * @return the length of the list
     */
    @Override
    public int getSize() {
        return documentList.size();
    }

    /**
     * Returns the value at the specified index.
     *
     * @param index the requested index
     * @return the value at <code>index</code>
     */
    @Override
    public Document getElementAt(int index) {
        return documentList.get(index);
    }

    /**
     * Use this function to add a new <code>Document</code> to the list.
     *
     * @param document The document which should be added
     * @see Document
     * @see #removeDocument(int) 
     */
    public void addDocument(Document document) {
        Objects.requireNonNull(document, "Null reference passed as document.");

        logger.info(String.format("Document [%s] added.", document.getFile().getName()));
        int index0 = getSize();
        documentList.add(document);
        fireIntervalAdded(this, index0, getSize());
    }

    /**
     * If you'd like to add the whole possible content of a directory at once instead
     * of file one ba one, pass a directory reference to this function.
     * Note: This function is not operating recursively. Recursion has to be implemented
     * by your own.
     *
     * @param directory The <code>File</code> representation of a directory
     */
    public void addDirectory(File directory) {
        Objects.requireNonNull(directory, "Null reference passed as document.");

        InputFilter filter = new InputFilter();
        for (final File fileEntry : Objects.requireNonNull(directory.listFiles())) {
            if (filter.accept(fileEntry) && fileEntry.isFile()) {
                addDocument(new Document(fileEntry));
            }
        }
    }

    /**
     * A formerly added document can be removed from the list by using this
     * function.
     * 
     * @param index The index of the document to be removed
     * @see #addDocument(Document)  
     */
    public void removeDocument(int index) {
        logger.info(String.format("Document at index [%d] [%s] removed", index, getElementAt(index).getFile().getName()));
        documentList.remove(index);
        fireIntervalRemoved(this, index, index);
    }

    /**
     * This function swaps the position of two elements identified by their index
     * inside the list. After the position change a change event is fired.
     *
     * @param index1 The first element
     * @param index2 The second element
     */
    public void swapDocumentPosition(int index1, int index2) {
        Collections.swap(documentList, index1, index2);
        fireContentsChanged(documentList, index1, index2);
    }

    /**
     * This function moves the element identified by the given index one position up.
     * If successful the new index of the element is returned, otherwise -1.
     *
     * @param index The index of the element to be moved upwards
     * @return The new index on success, -1 if it failed
     */
    public int moveDocumentUp(int index) {
        if(index > 0) {
            swapDocumentPosition(index, index-1);
            return index-1;
        }
        return -1;
    }

    /**
     * This function moves the element identified by the given index one position down.
     * If successful the new index of the element is returned, otherwise -1.
     *
     * @param index The index of the element to be moved downwards
     * @return The new index on success, -1 if it failed
     */
    public int moveDocumentDown(int index) {
        if(index < getSize()-1) {
            swapDocumentPosition(index, index+1);
            return index+1;
        }
        return -1;
    }

    /**
     * Use this function to produce the destination file. It will consists of a combination of all
     * files inside this list in the same order as listed.
     *
     * @param destinationFile A <code>File</code> reference to the destination file
     * @throws IOException Throws exception, if IO process fails
     */
    public void merge(File destinationFile, Lesson lesson) throws IOException {

        logger.info("Starting merging process.");
        PDFMergerUtility merger = new PDFMergerUtility();

        // Adding the cover sheet as first page
        merger.addSource(lesson.getCoverFile());
        // Adding the pdfs from the documents list
        for( int i=0; i < getSize(); i++ )
        {
            merger.addSource(Objects.requireNonNull(getElementAt(i)).getFile());
        }

        merger.setDestinationFileName(Objects.requireNonNull(destinationFile).getAbsolutePath());
        merger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
        logger.info("Merging process finished successfully.");
    }

    /**
     * Performs the overlay of the two documents.
     *
     * @param basePDF
     * @param overlayDoc
     * @throws IOException
     * @throws COSVisitorException
     *
    private static void doOverlay(PDDocument basePDF, PDDocument overlayDoc) {

        Overlay overlay = new Overlay();
        PDDocument output = overlay.overlay();
    }
    */
}
