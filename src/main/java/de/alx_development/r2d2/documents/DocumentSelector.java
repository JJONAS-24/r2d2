package de.alx_development.r2d2.documents;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Objects;
import java.util.logging.Logger;

public class DocumentSelector extends JPanel {

    protected Logger logger = Logger.getLogger(getClass().getName());

    private final DocumentListModel documentListModel = new DocumentListModel();
    private final JList<Document> documentList = new JList<>();

    public DocumentSelector() {
        super();

        documentList.setModel(documentListModel);

        JButton addFileButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("document-new.png"))));
        addFileButton.setToolTipText("Eine Datei hinzufügen");
        addFileButton.addActionListener(e -> {
            //Handle open button action.
            if (e.getSource() == addFileButton) {
                final JFileChooser fc = new JFileChooser();
                fc.addChoosableFileFilter(new InputFilter());
                fc.setAcceptAllFileFilterUsed(false);
                int returnVal = fc.showOpenDialog(DocumentSelector.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    documentListModel.addDocument(new Document(file));
                } else {
                    logger.finest("Open command cancelled by user.");
                }
            }
        });

        JButton addFolderButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("folder-new.png"))));
        addFolderButton.setToolTipText("Ein Verzeichnis hinzufügen");
        addFolderButton.addActionListener(e -> {
            //Handle open button action.
            if (e.getSource() == addFolderButton) {
                final JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fc.setAcceptAllFileFilterUsed(false);
                int returnVal = fc.showOpenDialog(DocumentSelector.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    documentListModel.addDirectory(file);
                } else {
                    logger.finest("Open command cancelled by user.");
                }
            }
        });

        JButton removeFileButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("document-revert.png"))));
        removeFileButton.setToolTipText("Eine Datei löschen");
        removeFileButton.addActionListener(e -> documentListModel.removeDocument(documentList.getSelectedIndex()));

        JButton moveFileUpButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("up.png"))));
        moveFileUpButton.setToolTipText("Dokument nach oben schieben");
        moveFileUpButton.addActionListener(e -> {
            if(!documentList.isSelectionEmpty()) {
                int index = getDocumentListModel().moveDocumentUp(documentList.getSelectedIndex());
                setSelectedIndex(index);
            }
        });

        JButton moveFileDownButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("down.png"))));
        moveFileDownButton.setToolTipText("Dokument nach unten schieben");
        moveFileDownButton.addActionListener(e -> {
            if(!documentList.isSelectionEmpty()) {
                int index = getDocumentListModel().moveDocumentDown(documentList.getSelectedIndex());
                setSelectedIndex(index);
            }
        });

        final JToolBar toolBar = new JToolBar("Still draggable");
        toolBar.setBorderPainted(true);
        toolBar.add(addFileButton);
        toolBar.add(addFolderButton);
        toolBar.addSeparator();
        toolBar.add(removeFileButton);
        toolBar.addSeparator();
        toolBar.add(moveFileUpButton);
        toolBar.add(moveFileDownButton);

        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(new JScrollPane(documentList), BorderLayout.CENTER);
    }

    /**
     * This function returns a reference to the <code>DocumentListModel</code> which
     * is represented by this <code>DocumentSelector</code>.
     *
     * @see DocumentListModel
     * @return A reference to the list model
     */
    public DocumentListModel getDocumentListModel() {
        return documentListModel;
    }

    /**
     * Use this function to set the selected index inside the list. If the index is out of bounds,
     * it will be silently ignored.
     *
     * @param index The index which should be selected
     */
    public void setSelectedIndex(int index) {
        if(index > -1 && index < getDocumentListModel().getSize())
            documentList.setSelectedIndex(index);
    }
}
