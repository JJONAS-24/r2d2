package de.alx_development.r2d2.documents;

/*-
 * #%L
 * R2D2
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * Copyright (c) 2021 Jonas Thiel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * #L%
 */

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.io.File;
import java.util.Objects;
import java.util.logging.Logger;

public class OutputSelector extends JPanel {

    protected Logger logger = Logger.getLogger(getClass().getName());

    private File file;
    private final JLabel fileChooserInfo = new JLabel();

    public OutputSelector() {
        super();
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        setFile(null);

        JButton openFileChooserButton = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getResource("document-output.png"))));
        openFileChooserButton.setToolTipText("Ausgabedatei auswählen");
        openFileChooserButton.addActionListener(e -> {
            final JFileChooser fc = new JFileChooser();
            fc.addChoosableFileFilter(new InputFilter());
            fc.setAcceptAllFileFilterUsed(false);
            int returnVal = fc.showOpenDialog(OutputSelector.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                setFile(fc.getSelectedFile());

            } else {
                logger.finest("Open command cancelled by user.");
            }
        });

        add(openFileChooserButton, BorderLayout.WEST);
        add(fileChooserInfo, BorderLayout.CENTER);
    }

    public File getFile() {
        return file;
    }

    private void setFile(File file) {
        String filename = "undefined";
        if(file != null) {
            filename = file.getAbsolutePath();
        }
        this.file = file;
        fileChooserInfo.setText(String.format("<html><style>p {margin: 2px 10px;}</style><p><b>Ausgabedatei:</b> <p style=\"color:blue;\">%s</p></p></html>", filename));
    }
}
